<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FirewallController extends Controller
{
    /**
    * Displays form to create a new request
    *
    * @return Response
    */
    public function index()
    {
        return view('firewall.create', ['status' => 'view']);
    }

    /**
    * Process form data and email a new request
    *
    * @param Request
    * @return Response
    */
    public function create(Request $request)
    {
        $this->validate($request, [
            'email'  => 'required|email|max:255',
            'fromIp' => 'required|ip|max:255',
            'toIp'   => 'required|ip|max:255',
            'ports'  => 'required|max:255',
            'reason' => 'required|max:255'
        ]);

        $contents = 'Reply To: ' . $request->email . "\r\n" . 'From IP: ' .
                    $request->fromIp . "\r\n" . 'To IP:' . $request->toIp .
                    "\r\n" . 'Ports: ' . $request->ports . "\r\n" . 'Reason: ' .
                    $request->reason;

        mail('skelso32@gmail.com', 'Firewall Request', $contents);

        return view('firewall.create', [
            'status'  => 'created',
            'message' => 'Your request has been submitted, updates will be delivered to: ' . $request->email
        ]);
    }
}
