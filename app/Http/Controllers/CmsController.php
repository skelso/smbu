<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CmsController extends Controller
{
    /**
    * Displays form to create a new request
    *
    * @return Response
    */
    public function index()
    {
        return view('cms.create', ['status' => 'view']);
    }

    /**
    * Process form data and email a new request
    *
    * @param Request
    * @return Response
    */
    public function create(Request $request)
    {
        if ($request->type == 'inplay')
        {
            $this->validate($request, [
                'email'   => 'required|email|max:255',
                'site'    => 'required|max:255',
                'screens' => 'required|max:255'
            ]);
        }
        else
        {
            $this->validate($request, [
                'email'   => 'required|email|max:255',
                'site'    => 'required|max:255',
                'screens' => 'required|max:255',
                'display' => 'required|max:255'
            ]);
        }

        if ($request->type == 'inplay')
        {
            $contents = 'Reply To:' . $request->email . "\r\n" . 'Site:' .
                        $request->site . "\r\n" . 'Number of Displays:' .
                        $request->screens . "\r\n";
        }
        else
        {
            $contents = 'Reply To: ' . $request->email . "\r\n" . 'Site: ' .
                        $request->site . "\r\n" . 'Number of Displays: ' .
                        $request->screens . "\r\n" . 'Display Type: ' .
                        $request->display;
        }

        mail('skelso32@gmail.com', 'CMS Request', $contents);

        return view('cms.create', [
            'status'  => 'created',
            'message' => 'Your request has been submitted, updates will be delivered to: ' . $request->email
        ]);
    }
}
