<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ReleaseController extends Controller
{
    /**
    * Displays form to create a new request
    *
    * @return Response
    */
    public function index()
    {
        return view('release.create', ['status' => 'view']);
    }

    /**
    * Process form data and email a new request
    *
    * @param Request
    * @return Response
    */
    public function create(Request $request)
    {
        $this->validate($request, [
            'email'       => 'required|email|max:255',
            'environment' => 'required|max:255',
            'project'     => 'required|max:255',
            'repos'       => 'required|max:255',
            'tag'         => 'required|max:255'
        ]);

        $contents = 'Reply To: ' . $request->email . "\r\n" . 'Environment: ' .
                    $request->environment . "\r\n" . 'Project: ' . $request->project .
                    "\r\n" . 'Repo: ' . $request->repos . "\r\n" . 'Version Tag: ' .
                    $request->tag;

        mail('skelso32@gmail.com', 'Release Request', $contents);

        return view('release.create', [
            'status'  => 'created',
            'message' => 'Your request has been submitted, updates will be delivered to: ' . $request->email
        ]);
    }
}
