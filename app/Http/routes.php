<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Task Routes\
	Route::get('/fwrequest', 'FirewallController@index');
	Route::post('/fwrequest', 'FirewallController@create');
	Route::get('/cmsrequest', 'CmsController@index');
    Route::post('/cmsrequest', 'CmsController@create');
    Route::get('/releaserequest', 'ReleaseController@index');
    Route::post('/releaserequest', 'ReleaseController@create');
