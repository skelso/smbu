@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <div class="alert alert-success @if($status == 'view')hide @endif">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>{{$message or ''}}</p>
        </div>

        <div class="well">
            <h3 class="text-center">CMS Deployment Request Form</h3>
            <div class="row">
                <div class="center-block">
                    <form action="{{ url('/cmsrequest') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Your Email Address</label>
                            <div class="col-sm-6">
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">Deployment Type</label>
                            <div class="col-sm-6">
                                <select name="type" id="type" class="form-control">
                                    <option value="">Select a Deployment Type</option>
                                    <option value="2.0">CMS 2.0 Machine Deployment</option>
                                    <option value="1.0">CMS 1.0 Machine Deployment</option>
                                    <option value="inplay">InPlay Deployment</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="site" class="col-sm-3 control-label">Site</label>
                            <div class="col-sm-6">
                                <input type="text" name="site" id="site" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="screens" class="col-sm-3 control-label">Number of Screens</label>
                            <div class="col-sm-6">
                                <input type="text" name="screens" id="screens" class="form-control">
                            </div>
                        </div>

                        <div class="form-group display-type">
                            <label for="display" class="col-sm-3 control-label">Display Type</label>
                            <div class="col-sm-6">
                                <select name="display" id="display" class="form-control">
                                    <option value="">Select a Display Type</option>
                                    <option value="HDMI">HDMI</option>
                                    <option value="DVI">DVI</option>
                                    <option value="VGA">VGA</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
$('#type').on('change', function() {
    if ($('#type').val() == 'inplay') {
        $('.display-type').hide();
    }
    else {
        $('.display-type').show();
    }
});
@endsection
