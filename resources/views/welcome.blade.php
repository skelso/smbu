@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h1 class="panel-title text-center">ABANDON ALL HOPE YE WHO ENTERS HERE</h1>
                </div>
                <div class="panel-body">
                    <p class="text-center">Screw Wordpress<p>
                    <div class="well">
                        <h3>Quick Links</h3>
                        <dl class="dl-horizontal">
                            <dt>Firewall Change Request:</dt>
                            <dd><a href="{{ url('/fwrequest') }}">Click Here</a></dd>
                            <dt>CMS Request:</dt>
                            <dd><a href="{{ url('/cmsrequest') }}">Click Here</a></dd>
                            <dt>Release Request:</dt>
                            <dd><a href="{{ url('/releaserequest') }}">Click Here</a></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
