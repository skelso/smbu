@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <div class="alert alert-success @if($status == 'view')hide @endif">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>{{$message or ''}}</p>
        </div>

        <div class="well">
            <h3 class="text-center">Release Request Form</h3>
            <div class="row">
                <div class="center-block">
                    <form action="{{ url('/releaserequest') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Your Email Address</label>
                            <div class="col-sm-6">
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="environment" class="col-sm-3 control-label">Environment</label>
                            <div class="col-sm-6">
                                <select name="environment" id="environment" class="form-control">
                                    <option value="">Select an Environment</option>
                                    <option value="dev">Dev</option>
                                    <option value="integ">Integ</option>
                                    <option value="preprod">Pre-Production</option>
                                    <option value="prod">Production</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="project" class="col-sm-3 control-label">Project</label>
                            <div class="col-sm-6">
                                <input type="text" name="project" id="project" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="repos" class="col-sm-3 control-label">Git Repo</label>
                            <div class="col-sm-6">
                                <select name="repos" id="repos" class="form-control">
                                    <option value="">Please Select a Git Repo</option>
                                    <option value="foo">Foo</option>
                                    <option value="bar">Bar</option>
                                    <option value="fubar">Fubar</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tag" class="col-sm-3 control-label">Version Tag</label>
                            <div class="col-sm-6">
                                <input type="text" name="tag" id="tag" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
