@extends('layouts.app')

@section('content')
    <div class="container">
        <!-- Display Validation Errors -->
        @include('common.errors')
        <div class="alert alert-success @if($status == 'view')hide @endif">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>{{$message or ''}}</p>
        </div>

        <div class="well">
            <h3 class="text-center">Firewall Request Form</h3>
            <div class="row">
                <div class="center-block">
                    <form action="{{ url('/fwrequest') }}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Your Email Address</label>
                            <div class="col-sm-6">
                                <input type="text" name="email" id="email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fromIp" class="col-sm-3 control-label">From IP</label>
                            <div class="col-sm-6">
                                <input type="text" name="fromIp" id="fromIp" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="toIp" class="col-sm-3 control-label">To IP</label>
                            <div class="col-sm-6">
                                <input type="text" name="toIp" id="toIp" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ports" class="col-sm-3 control-label">Ports Needed</label>
                            <div class="col-sm-6">
                                <input type="text" name="ports" id="ports" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="reason" class="col-sm-3 control-label">Reason Needed</label>
                            <div class="col-sm-6">
                                <textarea rows="5" class="form-control" id="reason" name="reason"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-3">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
